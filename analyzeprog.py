#! /usr/bin/python3
print("AOC 2019: Intcode function-analysis tool by CompuCat")
print("Assumes $01, $02 input and $00 output.")
print("WARNING: this makes some naive assumptions.")
print("----------------------------------------------------")

#Read input and do some setup
d=[int(i) for i in open("inputday2.txt","r").read().strip().split(",")]
d[1]=90
d[2]=74
#Set up a dictionary of "solutions", or functions describing memory addresses in terms of the two inputs and constants.
sol={}
sol[1]="IN1" #Placeholder text; prevents the two inputs from being considered constants.
sol[2]="IN2"

print()
print("Memory addresses as functions of inputs:")
#Run program and track memory locations as functions of the two inputs.
i=0
while i<len(d):
	if d[i]==99: break
	dest=d[i+3] #Protect against destination address being overwritten in memory.

	#Begin printing function to console
	print("$"+str(d[i+3])+"=",end="") 
	sol[dest]="("
	if d[i+1] in sol: sol[dest]+=sol[d[i+1]] #Search for existing solution to this memory address (is it already a function of some other memory addresses?)
	else: sol[dest]+=str(d[d[i+1]]) #Otherwise, naively assume it's a constant

	#Execute opcode
	if d[i]==1:
		sol[dest]+="+"
		d[d[i+3]]=d[d[i+1]]+d[d[i+2]]
	elif d[i]==2:
		sol[dest]+="*"
		d[d[i+3]]=d[d[i+1]]*d[d[i+2]]

	#Finish printing function to console; similar logic to above
	if d[i+2] in sol: sol[dest]+=sol[d[i+2]]
	else: sol[dest]+=str(d[d[i+2]])
	sol[dest]+=")"

	print(sol[dest])
	i+=4

print()
print("Input@ $01: "+str(d[1])+", "+str(d[2]))
print("Output@ $00: "+str(d[0]))
