#! /usr/bin/python3
#AOC D2P1 solution by CompuCat: a basic Intcode interpreter

print("AOC 2019 D2P1 solution by CompuCat: basic Intcode interpreter.")

#Load data from hardcoded file and preset values per problem statement
d=[int(i) for i in open("inputday2.txt","r").read().strip().split(",")]
d[1]=12
d[2]=2

#Begin main interpreter loop
i=0
while i<len(d):
	try:
		if d[i]==1: d[d[i+3]]=d[d[i+1]]+d[d[i+2]]
		elif d[i]==2: d[d[i+3]]=d[d[i+1]]*d[d[i+2]]
		elif d[i]==99: break
		i+=4
	except:
		try: #Nested exception handling because I'm lazy about bounds checking
			print("d="+str(d))
			print("opcode="+str(d[i]))
			print("arg1="+str(d[i+1]))
			print("arg2="+str(d[i+2]))
			print("arg3="+str(d[i+3]))
			print("d@a1="+str(d[d[i+1]]))
			print("d@a2="+str(d[d[i+2]]))
			print("d@a3="+str(d[d[i+3]]))
		except: pass

print("Input@ $01: "+str(d[1])+", "+str(d[2]))
print("Output@ $00: "+str(d[0]))
