#! /usr/bin/python3

print("AOC D3P1 2019 by CompuCat: Wire-crossing")
print("----------------------------------------")

#Read input and do some setup
lut={"L":(-1,0),"R":(1,0),"U":(0,1),"D":(0,-1)} #Lookup table from directions to vectors
#lines=[[(lut[j[0]], int(j[1:])) for j in i.split(",")] for i in open("d3input.txt","r").readlines()] #
lines=[[[k*int(j[1:]) for k in lut[j[0]]] for j in i.split(",")] for i in open("d3input.txt","r").readlines()] #
linepts=[[(0,0)],[(0,0)]]

print("Tracing lines...")
for l in range(2):
	for move in lines[l]:
			linepts[l].append((linepts[l][-1][0]+move[0], linepts[l][-1][1]+move[1]))
	linepts[l]=linepts[l][1:] #Remove the origin point.

p=sorted(list(set(linepts[0]).intersection(set(linepts[1]))), key=lambda p: abs(p[0])+abs(p[1]))[0]

print("Point is "+str(p)+", distance is "+str(abs(p[0])+abs(p[1])))
