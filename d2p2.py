#! /usr/bin/python3
import sympy, math

print("AOC D2P2 2019 by CompuCat: Intcode function-analysis (aka I really should make analyzeprog.py a library)")
print("Assumes $01, $02 input and $00 output.")
print("WARNING: this makes a *super* naive assumption that you can reduce this Intcode program to a linear")
print("function with integer solutions. This makes solution-finding trivial, but could break with non-trivial")
print("Intcode programs. You have been warned!")
print("--------------------------------------------------------------------------------------------------------")

#Read input and do some setup
d=[int(i) for i in open("inputday2.txt","r").read().strip().split(",")]
d[1]=90 #Inputs, used for initial simulation only.
d[2]=74
target=19690720 #Target value
#Set up a dictionary of "solutions", or functions describing memory addresses in terms of the two inputs and constants.
sol={}
sol[1]="x" #Placeholder text; prevents the two inputs from being considered constants.
sol[2]="y"

print()
print("Memory addresses as functions of inputs:")
#Run program and track memory locations as functions of the two inputs.
i=0
while i<len(d):
	if d[i]==99: break
	dest=d[i+3] #Protect against destination address being overwritten in memory.

	#Begin printing function to console
	print("$"+str(d[i+3])+"=",end="") 
	sol[dest]="("
	if d[i+1] in sol: sol[dest]+=sol[d[i+1]] #Search for existing solution to this memory address (is it already a function of some other memory addresses?)
	else: sol[dest]+=str(d[d[i+1]]) #Otherwise, naively assume it's a constant

	#Execute opcode
	if d[i]==1:
		sol[dest]+="+"
		d[d[i+3]]=d[d[i+1]]+d[d[i+2]]
	elif d[i]==2:
		sol[dest]+="*"
		d[d[i+3]]=d[d[i+1]]*d[d[i+2]]

	#Finish printing function to console; similar logic to above
	if d[i+2] in sol: sol[dest]+=sol[d[i+2]]
	else: sol[dest]+=str(d[d[i+2]])
	sol[dest]+=")"

	print(sol[dest])
	i+=4

# Use sympy to solve our equation.
x,y = sympy.symbols("x y")
fun=sympy.sympify(sol[0])-target

out=[0,0]
out[0]=math.floor(sympy.solve(fun.subs(y,0))[0]) #Assuming y=0, x gives a float solution.
out[1]=sympy.solve(fun.subs(x, out[0]))[0] #Rounding x down, we then solve for y and hope it's an int.

print("Solution: "+str(out))
#print("Input@ $01: "+str(d[1])+", "+str(d[2]))
#print("Output@ $00: "+str(d[0]))
